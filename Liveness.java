package student;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/*
 * Other classes may appear here
 * 
 */
class Triple {
	/*
	 * Single instance/period of liveness of a variable
	 */
	private String variable;
	private Integer start;
	private Integer end;
	
	Triple () {
		this.variable = "";
		this.start = 0;
		this. end = 0;
	}
	
	Triple (String var, Integer start, Integer end) {
		this.variable = var;
		this.start = start;
		this. end = end;
	}
	public String getVariable() {return variable;}
	public Integer getStart() {	return start;}
	public Integer getEnd() {return end;}	
	public void setVariable(String var) {this.variable = var;}
	public void setStart(Integer start) {this.start = start;}
	public void setEnd(Integer end) {this.end = end;}
	
}
public class Liveness {

	
	public TreeMap<String, Integer> generateSolution(String fInName) {
		// PRE: fInName is a valid input file
		// POST: returns a TreeMap mapping variables (String) to registers (Integer) 

		List<String> symbols = Arrays.asList("+", "-", "*", "/", ":=", "[", "]");

		// break down code into usable data
		List<String> textData = readFileToArray(fInName);
		List<String> varList = getVar(textData, symbols);
		List<Triple> livePeriodList = calcLiveness(textData, varList);
		
		if (varList.size() == 0) return null;	// no code
		
		TreeMap<String, List<String>> livenessClash = calculateLivenessClash(varList, livePeriodList);	
		
		varList = reorderByDegree(livenessClash);
		livenessClash = calculateLivenessClash(varList, livePeriodList);
		TreeMap<Integer, List<String>> regVarMap = new TreeMap<Integer, List<String>>();		
		TreeMap<String, Integer> solution = new TreeMap<String, Integer>();


		
		// Initialize with single empty register
		int regCounter = 1;
		regVarMap.put(regCounter, new ArrayList<String>());
		
		for (String var : varList) { // for each variable
			boolean isClash = false;
			List<String> varClashData = livenessClash.get(var);
			
			// for each existing register
			// clone TreeMap for iterating, allows updating of original			
			for (Entry<Integer, List<String>> register : new TreeMap<Integer, List<String>>(regVarMap).entrySet()){
				List<String> currentRegData = register.getValue();
				isClash = false;
				
				// for each variable assigned to current register
				for (String vInReg : currentRegData) {
					
					// compare liveness of unassigned variable to variable in register
					if (varClashData.contains(vInReg)) { // stop checking if liveness clashes
						isClash = true;
						break;	
					}
				}
				// done comparing to all variable in current register
				if (!isClash) {	// no clash in current register, add
					int curRegNum = register.getKey();
					List<String> curRegVars = regVarMap.get(register.getKey());
					if (!curRegVars.contains(var)) {
						curRegVars.add(var);
						regVarMap.put(curRegNum, curRegVars);
						break;	// next variable
					}
				}
				
			}
			// done checking all registers
			if (isClash) { // clash in every register, create new
				List<String> newReg = new ArrayList<String>();
				newReg.add(var);
				regVarMap.put(regCounter+1, newReg);
				regCounter++;
			}

		}
	
		
		// convert register-variable mapping into correct output format
		for (Entry<Integer, List<String>> register: regVarMap.entrySet()) {
			int regNum = register.getKey();
			List<String> vInReg = register.getValue();
			for (String var : vInReg) {
				solution.put(var, regNum);
			}	
		}
		
		return solution;
	}

	public void writeSolutionToFile(TreeMap<String, Integer> t, String solnName) {
		// PRE: t represents a valid register allocation
		// POST: the register allocation in t is written to file solnName
		
		// find number of registers used
		int regCount = 0;
		for (Entry<String, Integer> mapping : t.entrySet()) {
			regCount = Math.max(regCount, mapping.getValue());
		}
		
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(solnName, "UTF-8");
			writer.println(regCount);
			for (Entry<String, Integer> mapping : t.entrySet()) {
				writer.println(mapping.getKey() + " " + mapping.getValue());
			}

		}catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			writer.close();
		}
		
	}

	/**
	 * Generate liveness clash list/s between variables. 
	 * Finds neighbours of vertex/variable.
	 * 
	 * @param varList
	 * @param liveList
	 * @return TreeMap each variable and its neighbours
	 */
	private static TreeMap<String, List<String>> calculateLivenessClash(List<String> varList, List<Triple> liveList) {
		
		TreeMap<String, List<String>> livenessClash = new TreeMap<String, List<String>>();

		for (String var : varList) {
			List<Triple> allVarLiveInstance = getAllVarLiveInstance(var, liveList);
			List<String> liveClashWith = new ArrayList<String>();
 			Iterator<Triple> iter = liveList.iterator();
 			
 			// compare each variable to all lines
			while (iter.hasNext()) {
				Triple compareVar = iter.next();
				// not comparing to itself
				if (!compareVar.getVariable().equals(var)) {
					// not already compared
					if (!liveClashWith.contains(compareVar.getVariable())) {
						// compare live periods
						for (Triple varInstance : allVarLiveInstance) {
							if (isClash(compareVar, varInstance)) {
								liveClashWith.add(compareVar.getVariable());
							}				
						}
					}
				}
			}
			
			livenessClash.put(var, liveClashWith);
		}
		return livenessClash;
		
	}
	
	
	/**
	 * Determines if input variables have a liveness overlap
	 * 
	 * @param var1 Type triple
	 * @param var2 Type triple
	 * @return true if liveness overlaps, otherwise false
	 */
	private static boolean isClash (Triple var1, Triple var2) {
		
		int v1S = var1.getStart();
		int v1E = var1.getEnd();
		
		int v2S = var2.getStart();
		int v2E = var2.getEnd();
		
		
		// either start or end of var1 is within liveness period of var2
		if (v1S >= v2S && v1S <= v2E) {	return true; } // v2S < v1S < v2E
		if (v1E >= v2S && v1E <= v2E) {	return true; } // v2S < v1E < v2E
		
		if (v2S >= v1S && v2S <= v1E) {	return true; } // v1S < v2S < v1E
		if (v2E >= v1S && v2E <= v1E) {	return true; } // v1S < v2E < v1E
		return false;
	}
	
	
	/**
	 * Returns list containing all periods of liveness of given variable
	 * 
	 * @param var
	 * @param liveList
	 * @return
	 */
	private static List<Triple> getAllVarLiveInstance (String var, List<Triple> liveList) {
		List<Triple> allVarLiveInstance = new ArrayList<Triple>();
		for (Triple t : liveList) {
			if (t.getVariable().equals(var)) {
				allVarLiveInstance.add(t);
			}
		}
		return allVarLiveInstance;
		
	}
	
	
	/**
	 * Split lines in text file into an array
	 * 
	 * @param fInName
	 * @return text in array
	 */
	private static List<String> readFileToArray(String fInName){
		
		BufferedReader bufferReader = null;
		List<String> textData = new ArrayList<String>();
		try {
			bufferReader = new BufferedReader(new FileReader(fInName));
		    StringBuilder sb = new StringBuilder();
		    String line = bufferReader.readLine();
		    textData.add(line);
		    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = bufferReader.readLine();
		        textData.add(line);
		    }	
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bufferReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return textData;
	}
	
	
	/**
	 * Finds variables within code
	 * 
	 * @param textData
	 * @param symbols2 
	 * @return List of variables
	 */
	private static List<String> getVar(List<String> textData, List<String> symbols) {
		/*
		 * Method takes in array of symbols, changes into regex compliant string
		 * Handle "live-in" being split into 'live' and 'in' with "in" being stored as a variable
		 * 'in' case is hard coded
		 */
		
		String splitDelimiter = "";
		for(String str : symbols) {
			splitDelimiter += "\\" + str + "|";
		}
		splitDelimiter += " ";


		List<String> varList = new ArrayList<String>();
		
		
		for (String strLine : textData ) { // check all lines
			if (strLine != null) {
				// TODO
				// handle case for "live-in"
				
				
				String[] split = strLine.split(splitDelimiter);	// break lines up
				for (int i=0; i<split.length; i++) {
					if (split[i].length() == 2) {
						
						// strings of length 2 are the correct format to be a variable
						// first char is a letter
						if (Character.isLetter(split[i].charAt(0))) {	
							// second char is letter or digit
							if (Character.isLetter(split[i].charAt(1)) || Character.isDigit(split[i].charAt(1))) {
								// TODO
								// hard-coded edge case "in" from "live-in"
								if (!varList.contains(split[i]) && !split[i].equals("in")) {
									varList.add(split[i]);
								}
							}
						}
					}				
				}
			}
		}
	
		return varList;
	}
	
	
	/**
	 * Returns the number of the first preceding line where given variable is on the selected side
	 * true = LHS
	 * false = RHS
	 * 
	 * @param variable
	 * @param textData
	 * @param startSearchPos
	 * @param side
	 * @return int 
	 */
	private static int getPrecedingInstance (String variable, List<String> textData, int startSearchPos, boolean side) {
		

		
		String assignSym = new String(":=");
		String out = "live-out";
		String in = "live-in";
		
		
		for (int lineNum=startSearchPos-1; lineNum>=0; lineNum--) {
			String line = textData.get(lineNum);
			if (line != null) {
				int assignPos = line.indexOf(assignSym);
				if (assignPos >= 0 ) {
					
					String lhs = line.substring(0, assignPos);
					String rhs = line.substring(assignPos);
					
					// standard LHS / RHS check
					if (side && lhs.contains(variable)) {
						return lineNum;
					} else if (!side && rhs.contains(variable)) {
						return lineNum;
					} 
				}
				// line-in / line-out cases
				if (side && line.contains(in) && line.contains(variable)) {
					return lineNum;
				} else if (!side & line.contains(out) && line.contains(variable)) {
					return lineNum;
				}
				
			}
		}
		
		return -1;
		
	}

	
	/**
	 * Returns list containing period of liveness for each variable. Variables may have more than
	 * one period of liveness
	 * 
	 * @param textData
	 * @param varList
	 * @return type List<triple>
	 */
	private static List<Triple> calcLiveness(List<String> textData, List<String> varList) {
		// TODO Auto-generated method stub
		List<Triple> liveList = new ArrayList<Triple>();

		for (String var : varList) {
			int lineNum = textData.size();
			do {
				// separate code line into closest previous LHS and RHS matching variable var
				int lhs = getPrecedingInstance(var, textData, lineNum, true);
				int rhs = getPrecedingInstance(var, textData, lineNum, false);
				
				// LHS and RHS are valid line numbers
				if(lhs >=0 && rhs >=0) {
					Triple varLiveness = new Triple(var, lhs, rhs);
					liveList.add(varLiveness);
				}
				
				lineNum = lhs-1;	// skip to next previous line
			} while(lineNum>=0);

		}

		
		return liveList;
	}

	
	public static void main(String[] args)
	{

	}
	
	////////////////////////////
	private static List<String> reorderByDegree(TreeMap<String, List<String>> livenessClash) {
		// TODO Auto-generated method stub
		List<String> reorderedList = new ArrayList<String>(); 
		int loop = livenessClash.size();

		int maxDeg = 0;
		for (Entry<String, List<String>> var : livenessClash.entrySet()) {
			maxDeg = Math.max(maxDeg, var.getValue().size());
		}
		
		for (int i=0; i< loop; i++) {
			int degree = maxDeg+1;
			String lowestDegVar = "";
			
			for (Entry<String, List<String>> var : livenessClash.entrySet()) {
				int lDeg = var.getValue().size();
				if (lDeg < degree) { 
					degree = lDeg; 
					lowestDegVar = var.getKey();
				}
			}
			reorderedList.add(lowestDegVar);
			livenessClash.remove(lowestDegVar);
			
			
		}
		
		return reorderedList;
	}

	
}


